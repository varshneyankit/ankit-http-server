const fs = require("fs").promises;
const http = require("http");
const uuid = require("uuid");

const PORT = 3000;

const server = http.createServer(async function listener(request, response) {
  try {
    let htmlContent = await fs.readFile("./data/index.html", "utf-8");
    let jsonContent = await fs.readFile("./data/data.json", "utf-8");

    let splittedUrl = request.url.split("/");

    if (request.method == "GET" && splittedUrl[1] == "html") {
      response.write(htmlContent);
      response.end();
    } else if (request.method == "GET" && splittedUrl[1] == "json") {
      response.write(jsonContent);
      response.end();
    } else if (request.method == "GET" && splittedUrl[1] == "uuid") {
      let uuidValue = uuid.v4();

      response.write(JSON.stringify({ uuid: uuidValue }), null, 4);
      response.end();
    } else if (request.method == "GET" && splittedUrl[1] == "status") {
      let statusCode = parseInt(splittedUrl[2]);

      if (!isNaN(statusCode) && statusCode >= 100 && statusCode < 600) {
        response.writeHead(statusCode, { "Content-Type": "text/plain" });
        response.end(`Response with status code ${statusCode}.`);
      } else {
        response.end("Status code is not correct.");
      }
    } else if (request.method == "GET" && splittedUrl[1] == "delay") {
      let seconds = parseInt(splittedUrl[2]);

      if (!isNaN(seconds)) {
        setTimeout(() => {
          response.writeHead(200, { "Content-Type": "text/plain" });
          response.end(`Success response after ${seconds} seconds.`);
        }, seconds * 1000);
      } else {
        response.end("The given time delay is not correct");
      }
    } else {
      response.write("URL not found.");
      response.end();
    }

    console.log("Received a request.");
  } catch (error) {
    console.log("Error occured:");
    console.error(error);
  }
});

server.listen(PORT, function executor() {
  console.log(`Server is up and running on PORT ${PORT}`);
});
